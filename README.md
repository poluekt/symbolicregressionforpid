# SymbolicRegressionForPID

## Introduction

Symbolic regression is a machine learning method that searches the space of mathematical expressions to find the model that best fits a given dataset, both in terms of accuracy and simplicity. In this project, we are trying to apply symbolic regression technique, using the Python-based tools like pySR, to find the family of functions to parametrise distributions of high-statistics samples used for particle identification calibration at LHCb.

## Links

* [PySR package](https://github.com/MilesCranmer/PySR): symbolic regression 
* [uproot package](https://uproot.readthedocs.io/en/latest/): accessing ROOT files in Python
* "Selection and processing of calibration samples to measure the particle identification performance of the LHCb experiment in Run 2", [arXiv:1803.00824](https://arxiv.org/abs/1803.00824): The paper describing the procedures of PID calibration at LHCb
* Calibration data files in [CERNBox](https://cernbox.cern.ch/s/aNJefozySdzjhut)
